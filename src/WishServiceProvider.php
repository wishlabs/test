<?php

namespace Wish;

use Illuminate\Support\ServiceProvider;

class WishServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }
    
    public function boot()
    {
        //
    }
}