<?php

namespace Wish\Database\Models;

class Role extends BaseModel
{
    /**
     * The admin identifier
     *
     * @var string
     */
    const ADMIN = 'Administrator';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];
}