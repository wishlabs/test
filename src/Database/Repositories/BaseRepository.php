<?php

namespace Wish\Database\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository
{
    abstract function model();
    
    public function paginate($perPage = 10): LengthAwarePaginator
    {
        //
    }

    public function query(): Builder
    {
        //
    }
    
    public function create(array $data)
    {
        //
    }

    public function find(int $id)
    {
        //
    }

    public function delete(int $id): int
    {
        //
    }

    public function all(): Collection
    {
        //
    }
}