<?php

namespace Wish\Database\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface BaseInterface
{
    public function paginate($perPage = 10): LengthAwarePaginator;
    
    public function query(): Builder;
    
    public function create(array $data);
    
    public function find(int $id);
    
    public function delete(int $id): int;
    
    public function all(): Collection;
}